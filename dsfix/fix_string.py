#!/usr/bin/env python3

import sys
from ftlip import IniReader

path = sys.argv[1]
renderWidth = sys.argv[2]
renderHeight = sys.argv[3]

dsfix_ini = IniReader(path + "/DSfix.ini", ' ')

dsfix_ini.set("renderWidth", renderWidth)
dsfix_ini.set("renderHeight", renderHeight)

# I really doesn't recommended even enable this soap in the eyes
# Even with DXVK there is fps drop sometimes, especially in
# Garden of Dark Roots
dsfix_ini.set("dofBlurAmount", "o")

# Causes lifts bugs on weak processors, at the same time will improve
# performance on weak processors in some areas
dsfix_ini.set("unlockFPS", "1")

# Well, this isn't working with WINE anyways
dsfix_ini.set("disableCursor", "1")
dsfix_ini.set("captureCursor", "1")

# This is needed for some QoL textures overriding
dsfix_ini.set("enableTextureOverride", "1")

# Even tho it's not recommended, I have 0 issues with this option enabled
dsfix_ini.set("skipIntro", "1")

dsfix_ini.set("dinput8dllWrapper", "DarkSoulsInputCustomizer.dll")

dsfix_ini.update_file()


# Backspace is used by default for menu, so this is problem
dsfix_keys_ini = IniReader(path + "/DSfixKeys.ini", ' ')
dsfix_keys_ini.set("toggle30FPSLimit", "VK_F12")
dsfix_keys_ini.update_file()
